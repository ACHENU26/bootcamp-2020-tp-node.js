/*--- --- --- REQUIRE --- --- --- */

const express = require('express')
const app = express()
const axios = require('axios')
const bodyParser = require('body-parser')

app.listen(3000)

app.use(express.json())
app.use(express.urlencoded({
  extended: true
}))

/*---------------------------------------------------------------------------------------*/

// GET method route
app.get('/users', async function (req, res) {
  try {
    const { data } = await axios.get('https://jsonplaceholder.typicode.com/users');

    console.log({ data });
    res.send({ data })

  } catch (error) {
    res.send(error)
  }
})

/*---------------------------------------------------------------------------------------*/

// GET method route
app.get('/users', async function (req, res) {
  try {
    let id = req.params.id;
    const { data } = await axios.get("http://www.jsonplaceholder.typicode.com/users" + id);
    
    console.log({data});
    res.send({data});

  } catch (error) {
    res.send(error);
  }
})

/*---------------------------------------------------------------------------------------*/

// POST method route
app.post('/users', async function (req, res) {
  try {
    firstname: "FIRSTNAME"
    lastname: "Lastname"
    email: "mail@mail.com"

    console.log("user added");
    res.send("user added");

  } catch (error) {
    res.send(error);
  }
})

/*---------------------------------------------------------------------------------------*/

// PUT method route
app.put('/users/:id', async function (req, res) {
  try {
    let id = req.params.id;
    const { data } = await axios.get("http://www.jsonplaceholder.typicode.com/users"+ id,{
      firstname: "FIRSTNAME",
      lastname: "Lastname",
      age: 23
    });

    console.log({data});
    res.send({data});

  } catch (error) {
    res.send(error);
  }
})

/*---------------------------------------------------------------------------------------*/

// DELETE method route
app.delete('user/:id', async function (req, res) {
  try {
    let id = req / pqrqms.id;
    const { data } = await axios.get("http://www.jsonplaceholder.typicode.com/users" + id + '');

    console.log({ data });
    res.send("user deleted");

  } catch (error) {
    res.send(error);
  }
})

/*---------------------------------------------------------------------------------------*/

// GET method route
app.get('/users/:id/posts', async function (req, res) {
  try {
    let id = req.params.id;
    const { data } = await axios.get("https://jsonplaceholder.typicode.com/users/" + id + "/posts");

    console.log({ data });
    res.send({ data });

  } catch (error) {
    res.send(error);
  }
})

/*---------------------------------------------------------------------------------------*/

// GET method route
app.get('/users/:id/albums', async function (req, res) {
  try {

    let id = req.params.id;
    const { data } = await axios.get('https://jsonplaceholder.typicode.com/users/' + id + '/albums');

    console.log({ data });
    res.send({ data })

  } catch (error) {
    res.send(error);
  }
})

/*---------------------------------------------------------------------------------------*/

// GET method route
// Not finished yet 

/* 

app.get ('photos', async function (req, res){
  try {
    let id = req.pqrms.id;
    axios.get('https://jsonplaceholder.typicode.com/users/'+userId+'/albums');
    let albumId = req.params.albumId;
  
  
  } catch(error){
    res.send(err)
  }
})

*/